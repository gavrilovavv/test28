<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Models\Brand;
use App\Models\Car;
use App\Models\CarModel;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $cars = Car::with(['brand', 'model'])
            ->where('user_id', Auth::id())
            ->get();

        return view('index', [
            'cars' => $cars,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $brands = Brand::all();
        $models = CarModel::all();

        return view('form', [
            'brands' => $brands,
            'models' => $models,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CarRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CarRequest $request)
    {
        $car = new Car($request->validated());
        $car->user_id = Auth::id();
        $car->save();

        return redirect()->route('cars')->with('success', 'Car created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show(int $id)
    {
        $car = Car::whre('id', $id)
            ->where('user_id', Auth::id())
            ->firstOrFail();

        return view('view', [
            'car' => $car,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(int $id)
    {
        $car = Car::where('id', $id)
            ->where('user_id', Auth::id())
            ->firstOrFail();
        $brands = Brand::all();
        $models = CarModel::where('brand_id', $car->brand->id)->get();

        return view('form', [
            'car'    => $car,
            'brands' => $brands,
            'models' => $models,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CarRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(CarRequest $request, int $id)
    {
        $car = Car::where('id', $id)
            ->where('user_id', Auth::id())
            ->firstOrFail();
        $car->update($request->validated());

        return redirect()->route('cars')->with('success', 'Car updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(int $id)
    {
        $car = Car::where('id', $id)
            ->where('user_id', Auth::id())
            ->firstOrFail();
        $car->delete();

        return redirect()->route('cars')->with('success', 'Car deleted successfully');
    }

    /**
     * @param int $brand_id
     * @return mixed
     */
    public function getModels(int $brand_id)
    {
        $models = CarModel::where('brand_id', $brand_id)->get();

        return response()->json($models);
    }
}
