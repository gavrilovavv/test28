(() => {
    const brandSelect = document.getElementById('brand');
    const modelSelect = document.getElementById('model');

    brandSelect.addEventListener('change', function () {
        let brandId = this.value;

        if (brandId) {
            fetch('/get-models/' + brandId)
                .then(response => response.json())
                .then(models => {
                    modelSelect.innerHTML = '';

                    models.forEach(model => {
                        const modelOption = document.createElement('option');

                        modelOption.value = model.id;
                        modelOption.text = model.name;

                        modelSelect.appendChild(modelOption);
                    })
                })
                .catch((error) => {
                    console.error('Failed to fetch to models list. ', error);
                })
        }
    });
})();
