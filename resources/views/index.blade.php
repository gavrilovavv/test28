<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>List of cars</title>
</head>
<body>
<div class="container">
    <h1>List of cars</h1>
    <a href="{{ route('cars.create') }}">Create</a>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Brand</td>
            <td>Model</td>
            <td>Year</td>
            <td>Mileage</td>
            <td>Color</td>
            <td>Actions</td>
        </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr>
                <td><a href="{{ route('cars.show', $car->id) }}">{{ $car->id }}</a></td>
                <td>{{ $car->brand->name }}</td>
                <td>{{ $car->model->name }}</td>
                <td>{{ $car->year }}</td>
                <td>{{ $car->mileage }}</td>
                <td>{{ $car->color }}</td>
                <td>
                    <a href="{{ route('cars.edit', $car->id) }}">Edit</a>
                    <form action="{{ route('cars.destroy', $car->id) }}" method="POST" style="display:inline;">
                        @csrf
                        @method('DELETE')
                        <button type="submit">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
