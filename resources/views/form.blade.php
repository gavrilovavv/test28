<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @php
        /** @var \App\Models\Car $car */
        $new = empty($car);
    @endphp
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $new ? 'Create' : 'Edit' }} car</title>
</head>
<body>
<div class="container">
    <h1>{{ $new ? 'Create' : 'Edit' }} car</h1>
    <form id="carForm" action="{{ $new ? route('cars.store') : route('cars.update', $car->id) }}" method="POST">
        @csrf
        @if (!$new)
            @method('PUT')
        @endif

        @if($errors->any())
            @foreach($errors->all() as $error)
            <p style="color: red">{{ $error }}</p>
            @endforeach
        @endif

        <div>
            <label for="brand">Brand:</label>
            <select name="brand_id" id="brand">
                <option value="">Select brand</option>
                @foreach($brands as $brand)
                <option value="{{ $brand->id }}" {{ old('brand_id', !$new && $brand->id == $car->brand_id ? 'selected' : '') }}>
                    {{ $brand->name }}
                </option>
                @endforeach
            </select>
        </div>

        <div>
            <label for="model">Model:</label>
            <select name="model_id" id="model">
                <option value="">Select model</option>
                @if(!$new)
                    @foreach($models as $model)
                        <option value="{{ $model->id }}" {{ old('model_id', !$new && $model->id == $car->model_id ? 'selected' : '') }}>
                            {{ $model->name }}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>

        <div>
            <label for="year">Year:</label>
            <input type="text" id="year" name="year" value="{{ old('year', $new ? '' : $car->year) }}">
        </div>

        <div>
            <label for="mileage">Mileage:</label>
            <input type="text" id="mileage" name="mileage" value="{{ old('mileage', $new ? '' : $car->mileage) }}">
        </div>

        <div>
            <label for="color">Color:</label>
            <input type="text" id="color" name="color" value="{{ old('color', $new ? '' : $car->color) }}">
        </div>

        <button type="submit">Save</button>
    </form>
</div>
<script src="{{ URL::asset('js/common.js') }}?v{{ filemtime(public_path('js/common.js')) }}"></script>
</body>
</html>
