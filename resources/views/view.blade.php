<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>List of cars</title>
</head>
<body>
<div class="container">
    <h1>{{ $car->brand->name}} {{ $car->model->name }}</h1>
    <a href="{{ route('cars') }}">Back</a>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Brand</td>
            <td>Model</td>
            <td>Year</td>
            <td>Mileage</td>
            <td>Color</td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{{ $car->id }}</td>
            <td>{{ $car->brand->name }}</td>
            <td>{{ $car->model->name }}</td>
            <td>{{ $car->year }}</td>
            <td>{{ $car->mileage }}</td>
            <td>{{ $car->color }}</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
